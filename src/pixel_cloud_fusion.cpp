
/*
 * Copyright 2018-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************
 *  v1.0: amc-nu (abrahammonrroy@yahoo.com)
 *
 * pixel_cloud_fusion.cpp
 *
 *  Created on: May, 19th, 2018
 */

#include "pixel_cloud_fusion/pixel_cloud_fusion.h"
#include "tf_conversions/tf_eigen.h"
pcl::PointXYZI
ROSPixelCloudFusionApp::TransformPoint(const pcl::PointXYZI &in_point, const tf::StampedTransform &in_transform)
{
  tf::Vector3 tf_point(in_point.x, in_point.y, in_point.z);
  tf::Vector3 tf_point_t = in_transform * tf_point;
  pcl::PointXYZI p;
  p.x = tf_point_t.x();
  p.y = tf_point_t.y();
  p.z = tf_point_t.z();
  p.intensity = in_point.intensity;
  return p;
}

void ROSPixelCloudFusionApp::ImageCallback(const sensor_msgs::Image::ConstPtr &in_image_msg)
{
  if (!camera_info_ok_)
  {
    ROS_INFO("[%s] Waiting for Intrinsics to be available.", __APP_NAME__);
    return;
  }
  if (processing_)
    return;

  cv_bridge::CvImagePtr cv_image = cv_bridge::toCvCopy(in_image_msg, "bgr8");
  current_frame_ = cv_image->image;

  //cv::Mat undistorted_image;
  //cv::undistort(in_image, current_frame_, camera_intrinsics_, distortion_coefficients_);

  image_frame_id_ = in_image_msg->header.frame_id;
  image_size_.height = current_frame_.rows;
  image_size_.width = current_frame_.cols;
}

// tf_eigen.h only supports eigen double matrix (affine and isometry)
void tfToEigen4f(const tf::Transform & t, Eigen::Matrix<float, 4,4> & m)
{
  for (int i = 0; i < 3; i++) {
    m(i,3) = t.getOrigin()[i];
    for (int j = 0; j <3; j++) {
      m(i,j) = t.getBasis()[i][j];
    }
  }
  for (int col = 0; col < 3; col++) {
    m(3,col) = 0.0f;
  }
  m(3,3) = 1.0f;
}

void ROSPixelCloudFusionApp::CloudCallback(const sensor_msgs::PointCloud2::ConstPtr &in_cloud_msg)
{
  if (current_frame_.empty() || image_frame_id_.empty())
  {
    ROS_INFO("[%s] Waiting for Image frame to be available.", __APP_NAME__);
    return;
  }
  if (!camera_lidar_tf_ok_)
  {
    camera_lidar_tf_ = FindTransform(image_frame_id_,
                                     in_cloud_msg->header.frame_id);
  }
  if (!camera_info_ok_ || !camera_lidar_tf_ok_)
  {
    ROS_INFO("[%s] Waiting for Camera-Lidar TF and Intrinsics to be available.", __APP_NAME__);
    return;
  }
  if (publisher_overlay_depth_.getNumSubscribers() <= 0
  && publisher_fused_cloud_.getNumSubscribers() <=0
  && publisher_overlay_intensity_.getNumSubscribers() <=0) {
    ROS_INFO_THROTTLE(1, "No subscribers, skipping");
    return;
  }

  pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::fromROSMsg(*in_cloud_msg, *in_cloud);
  pcl::PointCloud<pcl::PointXYZI>::Ptr cam_cloud(new pcl::PointCloud<pcl::PointXYZI>);
  cam_cloud->points.resize(in_cloud->points.size());

  auto start = std::chrono::high_resolution_clock::now();

  cv::Mat intensity_image = current_frame_.clone();
  cv::Mat depth_image = current_frame_.clone();


  *cam_cloud = *in_cloud;

  // try mapping manually to take advatage of march-native flag
  // auto cam_cloud_e_full = cam_cloud->getMatrixXfMap (sizeof (pcl::PointXYZI) / sizeof (float),  sizeof (pcl::PointXYZI) / sizeof (float), 0);
  // map point cloud data to Eigen matrix (still can access through point cloud ptr)

  auto cam_cloud_e_full = cam_cloud->getMatrixXfMap();
  auto cam_cloud_e = cam_cloud_e_full.topRows(4);

  // generate a Eigen transformation matrix and apply to input cloud
  Eigen::Matrix<float, 4, 4> camera_lidar_tf_matrix;
  tfToEigen4f(camera_lidar_tf_, camera_lidar_tf_matrix);
  cam_cloud_e.noalias() = camera_lidar_tf_matrix*cam_cloud_e;

  // scale points according to 1/Z and then apply camera maxtrix to project
  auto scaled = cam_cloud_e.topRows(3) * (cam_cloud_e.row(2).cwiseInverse()).asDiagonal();
  auto img_pts = (C_ * scaled).eval();

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr out_cloud(new pcl::PointCloud<pcl::PointXYZRGB>); 
  out_cloud->points.clear();


  #pragma omp parallel default(none) shared(img_pts, cam_cloud, in_cloud, intensity_image, depth_image, out_cloud, current_frame_)
  #pragma omp for
  for (size_t i = 0; i < in_cloud->points.size(); i++)
  {
    int u = int(img_pts(0, i));
    int v = int(img_pts(1, i));

    if ((u >= 0) && (u < image_size_.width)
        && (v >= 0) && (v < image_size_.height)
        && cam_cloud->points[i].z > 0
      )
    {
      cv::Vec3b rgb_pixel = current_frame_.at<cv::Vec3b>(v, u);
      pcl::PointXYZRGB colored_3d_point;

      colored_3d_point.x = in_cloud->points[i].x;
      colored_3d_point.y = in_cloud->points[i].y;
      colored_3d_point.z = in_cloud->points[i].z;
      colored_3d_point.r = rgb_pixel[2];
      colored_3d_point.g = rgb_pixel[1];
      colored_3d_point.b = rgb_pixel[0];

      const tinycolormap::Color depth_color = tinycolormap::GetColor(cam_cloud->points[i].z / max_depth_,
                                                                     depth_colormap_type_);
      const tinycolormap::Color intensity_color = tinycolormap::GetColor(
        cam_cloud->points[i].intensity / max_intensity_, intensity_colormap_type_);
      #pragma omp critical
      {
        out_cloud->points.emplace_back(colored_3d_point);
        cv::circle(intensity_image, cv::Point(u, v), circle_size_,
                   cv::Scalar(intensity_color.bi(), intensity_color.gi(), intensity_color.ri()), cv::FILLED);
        cv::circle(depth_image, cv::Point(u, v), circle_size_,
                   cv::Scalar(depth_color.bi(), depth_color.gi(), depth_color.ri()), cv::FILLED);
      }
    }
  }
  cv_bridge::CvImage depth_ros, intensity_ros;
  depth_ros.header   = in_cloud_msg->header;
  depth_ros.encoding = sensor_msgs::image_encodings::BGR8;
  depth_ros.image    = depth_image;
  publisher_overlay_depth_.publish(depth_ros);
  intensity_ros.header = in_cloud_msg->header;
  intensity_ros.encoding = sensor_msgs::image_encodings::BGR8;
  intensity_ros.image = intensity_image;
  publisher_overlay_intensity_.publish(intensity_ros);

  auto elapsed = std::chrono::high_resolution_clock::now() - start;
  auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
  ROS_DEBUG_STREAM_THROTTLE(1, "Projection [ms]: " << milliseconds);

  // Publish PC
  sensor_msgs::PointCloud2 cloud_msg;
  pcl::toROSMsg(*out_cloud, cloud_msg);
  cloud_msg.header = in_cloud_msg->header;
  publisher_fused_cloud_.publish(cloud_msg);
}

void ROSPixelCloudFusionApp::IntrinsicsCallback(const sensor_msgs::CameraInfo &in_message)
{
  image_size_.height = in_message.height;
  image_size_.width = in_message.width;

  camera_intrinsics_ = cv::Mat(3, 3, CV_64F);
  for (int row = 0; row < 3; row++)
  {
    for (int col = 0; col < 3; col++)
    {
      camera_intrinsics_.at<double>(row, col) = in_message.K[row * 3 + col];
    }
  }

  distortion_coefficients_ = cv::Mat(1, 5, CV_64F);
  for (int col = 0; col < 5; col++)
  {
    distortion_coefficients_.at<double>(col) = in_message.D[col];
  }

  fx_ = static_cast<float>(in_message.P[0]);
  fy_ = static_cast<float>(in_message.P[5]);
  cx_ = static_cast<float>(in_message.P[2]);
  cy_ = static_cast<float>(in_message.P[6]);
  if(fx_ <=0 || fy_ <=0 || cx_ <=0 || cy_ <=0)
  {
    ROS_ERROR_STREAM("Invalid Intrinsics provided");
    camera_info_ok_ = false;
    return;
  }
  C_ << fx_, 0, cx_,
    0, fy_, cy_,
    0, 0, 1;
  intrinsics_subscriber_.shutdown();
  camera_info_ok_ = true;
  ROS_INFO("[%s] CameraIntrinsics obtained.", __APP_NAME__);
}

tf::StampedTransform
ROSPixelCloudFusionApp::FindTransform(const std::string &in_target_frame, const std::string &in_source_frame)
{
  tf::StampedTransform transform;

  camera_lidar_tf_ok_ = false;
  try
  {
    transform_listener_->lookupTransform(in_target_frame, in_source_frame, ros::Time(0), transform);
    camera_lidar_tf_ok_ = true;
    ROS_INFO("[%s] Camera-Lidar TF obtained", __APP_NAME__);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("[%s] %s", __APP_NAME__, ex.what());
  }

  return transform;
}

void ROSPixelCloudFusionApp::InitializeROSIo(ros::NodeHandle &in_private_handle)
{
  //get params
  std::string points_src, image_src, camera_info_src, fused_topic_str = "points_fused";
  std::string name_space_str = ros::this_node::getNamespace();

  ROS_INFO("[%s] This node requires: Registered TF(Lidar-Camera), CameraInfo, Image, and PointCloud.", __APP_NAME__);
  in_private_handle.param<std::string>("points_src", points_src, "/points_raw");
  ROS_INFO("[%s] points_src: %s", __APP_NAME__, points_src.c_str());

  in_private_handle.param<std::string>("image_src", image_src, "/image_rectified");
  ROS_INFO("[%s] image_src: %s", __APP_NAME__, image_src.c_str());

  in_private_handle.param<std::string>("camera_info_src", camera_info_src, "/camera_info");
  ROS_INFO("[%s] camera_info_src: %s", __APP_NAME__, camera_info_src.c_str());

  in_private_handle.param<double>("max_depth", max_depth_, 20.);
  ROS_INFO("[%s] max_depth: %.1f", __APP_NAME__, max_depth_);

  in_private_handle.param<double>("max_intensity", max_intensity_, 200.);
  ROS_INFO("[%s] max_intensity: %.1f", __APP_NAME__, max_intensity_);

  in_private_handle.param<int>("circle_size", circle_size_, 3);
  ROS_INFO("[%s] circle_size: %d", __APP_NAME__, circle_size_);

  int color_map;
  in_private_handle.param<int>("circle_size", circle_size_, 3);
  ROS_INFO("[%s] circle_size: %d", __APP_NAME__, circle_size_);

  //generate subscribers and sychronizers
  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, camera_info_src.c_str());
  intrinsics_subscriber_ = node_handle_.subscribe(camera_info_src,
                                                       1,
                                                       &ROSPixelCloudFusionApp::IntrinsicsCallback, this);

  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, image_src.c_str());
  cloud_subscriber_ = node_handle_.subscribe(image_src,
                                                  1,
                                                  &ROSPixelCloudFusionApp::ImageCallback, this);
  ROS_INFO("[%s] Subscribing to... %s", __APP_NAME__, points_src.c_str());
  image_subscriber_ = node_handle_.subscribe(points_src,
                                                  1,
                                                  &ROSPixelCloudFusionApp::CloudCallback, this);

  publisher_fused_cloud_ = node_handle_.advertise<sensor_msgs::PointCloud2>(fused_topic_str, 1);
  ROS_INFO("[%s] Publishing fused pointcloud in %s", __APP_NAME__, fused_topic_str.c_str());

  std::string overlay_intensity = "overlay_intensity";
  publisher_overlay_intensity_ = node_handle_.advertise<sensor_msgs::Image>(overlay_intensity, 1);
  ROS_INFO("[%s] Publishing intensity overlay in %s", __APP_NAME__, overlay_intensity.c_str());

  std::string overlay_depth = "overlay_depth";
  publisher_overlay_depth_ = node_handle_.advertise<sensor_msgs::Image>(overlay_depth, 1);
  ROS_INFO("[%s] Publishing depth overlay in %s", __APP_NAME__, overlay_depth.c_str());
  int intensity_color, depth_color;
  in_private_handle.param<int>("intensity_colormap", intensity_color, 2);
  ROS_INFO("[%s] intensity_colormap: %d", __APP_NAME__, intensity_color);
  in_private_handle.param<int>("depth_colormap", depth_color, 6);
  ROS_INFO("[%s] depth_colormap: %d", __APP_NAME__, depth_color);

  intensity_colormap_type_ = GetColotMapFromInt(intensity_color);
  depth_colormap_type_ = GetColotMapFromInt(depth_color);

  reconfig_server_ = boost::make_shared < dynamic_reconfigure::Server < pixel_cloud_fusion::PixelCloudFusionConfig > > (in_private_handle);
  dynamic_reconfigure::Server<pixel_cloud_fusion::PixelCloudFusionConfig>::CallbackType f = boost::bind(
      &ROSPixelCloudFusionApp::ReconfigureCallback, this, _1, _2);
  reconfig_server_->setCallback(f);
}

tinycolormap::ColormapType ROSPixelCloudFusionApp::GetColotMapFromInt(int color_int)
{
  tinycolormap::ColormapType colormap;
  switch (color_int) {
    case 0://Parula
      colormap = tinycolormap::ColormapType::Parula;
      break;
    case 1://Heat
      colormap = tinycolormap::ColormapType::Heat;
      break;
    case 2://Jet
      colormap = tinycolormap::ColormapType::Jet;
      break;
    case 3://Turbo
      colormap = tinycolormap::ColormapType::Turbo;
      break;
    case 4://Hot
      colormap = tinycolormap::ColormapType::Hot;
      break;
    case 5://Gray
      colormap = tinycolormap::ColormapType::Gray;
      break;
    case 6://Magma
      colormap = tinycolormap::ColormapType::Magma;
      break;
    case 7://Inferno
      colormap = tinycolormap::ColormapType::Inferno;
      break;
    case 8://Plasma
      colormap = tinycolormap::ColormapType::Plasma;
      break;
    case 9://Viridis
      colormap = tinycolormap::ColormapType::Viridis;
      break;
    case 10://Cividis
      colormap = tinycolormap::ColormapType::Cividis;
      break;
    default:
      colormap = tinycolormap::ColormapType::Turbo;
      break;
  }
  return colormap;
}

void ROSPixelCloudFusionApp::ReconfigureCallback(pixel_cloud_fusion::PixelCloudFusionConfig &config,
                                            uint32_t level)
{
  if(config.max_depth != max_depth_)
  {
    max_depth_ = config.max_depth;
    ROS_INFO("Max Depth: %.2f", max_depth_);
  }
  if(config.max_intensity != max_intensity_)
  {
    max_intensity_ = config.max_intensity;
    ROS_INFO("Max intensity: %.2f", max_intensity_);
  }
  if(config.circle_size != circle_size_)
  {
    circle_size_ = config.circle_size;
    ROS_INFO("Circle Size: %d", circle_size_);
  }
  tinycolormap::ColormapType config_intensity_colormap = GetColotMapFromInt(config.intensity_colormap);
  ROS_INFO("ColorMap: %d", config.intensity_colormap);
  if(config_intensity_colormap != intensity_colormap_type_)
  {
    intensity_colormap_type_ = config_intensity_colormap;
  }
  tinycolormap::ColormapType config_depth_colormap = GetColotMapFromInt(config.depth_colormap);
  ROS_INFO("ColorMap: %d", config.depth_colormap);
  if(config_depth_colormap != depth_colormap_type_)
  {
    depth_colormap_type_ = config_depth_colormap;
  }
}

  void ROSPixelCloudFusionApp::Run()
{
  ros::NodeHandle private_node_handle("~");
  tf::TransformListener transform_listener;

  transform_listener_ = &transform_listener;

  InitializeROSIo(private_node_handle);

  ROS_INFO("[%s] Ready. Waiting for data...", __APP_NAME__);

  ros::spin();

  ROS_INFO("[%s] END", __APP_NAME__);
}

ROSPixelCloudFusionApp::ROSPixelCloudFusionApp()
{
  camera_lidar_tf_ok_ = false;
  camera_info_ok_ = false;
  processing_ = false;
  image_frame_id_ = "";
}
